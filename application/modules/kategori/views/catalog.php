<div class="container-fluid">
  <div class="xnav">
    <nav class="nav">
      <div class="nav" id="nav-tab" role="tablist">
        <ul class="navbar-nav">
          <li class="dropdown"><a class="nav-link active" id="nav-shop-tab" data-toggle="tab" href="#nav-shop" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-archive"></i> shop</a>
            <ul class="isi-dropdown">
                  <li id="totle"><a href="#">T-shirt</a></li>
                  <li id="totle"><a href="#">Shirt</a></li>
                  <li id="totle"><a href="#">Outwear</a></li>
                  <li id="totle"><a href="#">Footwear</a></li>
                  <li id="totle"><a href="#">Pants</a></li>
                  <li id="totle"><a href="#">Accesories</a></li>
            </ul>
          </li>
        </ul>
        <a class="nav-link active" id="nav-shop-tab" data-toggle="tab" href="#nav-shop" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-archive"></i> catalogue</a>
        <a class="nav-link" id="nav-price-tab" data-toggle="tab" href="#nav-price" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="fa fa-tags"></i> special price</a>
        <a class="nav-link" id="nav-account-tab" data-toggle="tab" href="#nav-account" role="tab" aria-controls="nav-contact" aria-selected="false"><i class="fa fa-user-circle"></i> my account</a>
        <a class="nav-link" id="nav-bags-tab" data-toggle="tab" href="#nav-bags" role="tab" aria-controls="nav-contact" aria-selected="false"><i class="fa fa-shopping-basket"></i> bags</a>
      </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-shop" role="tabpanel" aria-labelledby="nav-shop-tab">
        <div class="container sksd">
          <div class="row">
          <link rel="stylesheet" href="<?php echo base_url(); ?>asset/style.css">
            <div class="col-md-3">
					    <div class="view view-ninth" >
                <a href="<?php echo base_url(); ?>kategori/tee">
                  <img src="<?php echo base_url(); ?>asset/images/tshirt.jpg" class="img-responsive" alt="" style="width:270px; height:220px;-webkit-filter:brightness(36%);"/>
                  <div class="mask mask-1"> </div>
                  <div class="mask mask-2"> </div>
                  <div class="content">
                    <h2>T-SHIRT</h2>
                  </div>
                </a> 
              </div>
				    </div>
            <div class="col-md-3">
					    <div class="view view-ninth" >
                <a href="surya.php">
                  <img src="<?php echo base_url(); ?>asset/images/shirt.jpg" class="img-responsive" alt="" style="width:270px; height:220px; -webkit-filter:brightness(36%); "/>
                  <div class="mask mask-1"> </div>
                  <div class="mask mask-2"> </div>
                  <div class="content">
                    <h2>SHIRT</h2>
                  </div>
                </a> 
              </div>
				    </div>
            <div class="col-md-3">
					    <div class="view view-ninth" >
                <a href="surya.php">
                  <img src="<?php echo base_url(); ?>asset/images/jacket.jpg" class="img-responsive" alt="" style="width:270px; height:220px;-webkit-filter:brightness(36%);"/>
                  <div class="mask mask-1"> </div>
                  <div class="mask mask-2"> </div>
                  <div class="content">
                    <h2>JACKET</h2>
                  </div>
                </a> 
              </div>
				    </div>
            <div class="col-md-3">
					    <div class="view view-ninth" >
                <a href="surya.php">
                  <img src="<?php echo base_url(); ?>asset/images/swt.jpg" class="img-responsive" alt="" style="width:270px; height:220px;-webkit-filter:brightness(36%);"/>
                  <div class="mask mask-1"> </div>
                  <div class="mask mask-2"> </div>
                  <div class="content">
                    <h2>SWEATER</h2>
                  </div>
                </a> 
              </div>
				    </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-price" role="tabpanel" aria-labelledby="nav-price-tab">

      </div>
      <div class="tab-pane fade" id="nav-account" role="tabpanel" aria-labelledby="nav-account-tab">

      </div>
      <div class="tab-pane fade" id="nav-bags" role="tabpanel" aria-labelledby="nav-bags-tab">

      </div>
    </div>
  </div>
</div>