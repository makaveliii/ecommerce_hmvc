<?php

class Kategori extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('status') != 'login') {
            redirect('auth');
        }
        $this->load->model('Kat_mods', 'kk');
    }

    function index()
    {
        $data['shop'] = $this->kk->getShop();
        $this->load->view('template/header');
        $this->load->view('index', $data);
        $this->load->view('index_js');
        $this->load->view('template/footer');
    }

    function tee()
    {
        $this->load->view('template/header');
        $this->load->view('catalog');
        $this->load->view('template/footer');
    }

}