<?php

class Auth extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Auth_model');
    }

    function index()
    {

        $this->form_validation->set_rules('username', 'username ','required');
        $this->form_validation->set_rules('password', 'password','required');
        if ($this->form_validation->run()==false) {
            $title = array(
                'tittle' => 'no'
            );
            $this->load->view('template/header-login');
            $this->load->view('index',$title);
            $this->load->view('template/footer-login');

        }else{
            $this->proses_login();
        }
    }

    function proses_login()
    {
        error_reporting(0);
        $username = htmlspecialchars($this->input->post('username'));
        $password = htmlspecialchars(md5($this->input->post('password')));

        $d = array(
            'username' => $username,
            'password' => $password
        );

        $cek_pass = $this->Auth_model->cek_user('user',$d)->num_rows();
        $cek_akses = $this->Auth_model->cek_akses($username,$password);
        $akses = $cek_akses[0]['akses'];
        $nama = $cek_akses[0]['nama'];
        $id = $cek_akses[0]['user_id'];
        $fire = $cek_akses[0]['fire'];

        if ($cek_pass > 0) {
            if ($akses == '2') {
                $sess_data = array(
                    'username' => $nama,
                    'status' => 'login'
                );

                $this->session->set_userdata($sess_data);
                echo "<script>alert('Berhasil Login');document.location='" . base_url('dashboard') . "'</script>";
                // redirect('dashboard');
            }else if($akses == 1 && $fire == null){
                redirect('auth/reset_pass/'.$id);
            }
        }else{
            echo "<script>alert('Data yang anda masukan tidak terdaftar');document.location='" . base_url('auth') . "'</script>";
        }
    }
    
    function reset_pass()
    {
        $id_s = $this->uri->segment('3');

        
        $this->form_validation->set_rules('username', 'username ','required');
        // $this->form_validation->set_rules('password', 'password','required');
        $this->form_validation->set_rules('new_password', 'new_password','required');
        if ($this->form_validation->run()==false) {
            $title = array(
                'tittle' => 'change'
            );
            $get_ui['lumia'] = $this->Auth_model->get_uid($id_s);
        $this->load->view('template/header-login');
        $this->load->view('ch_pass',$get_ui);
        $this->load->view('template/footer-login');


        }else{
            $this->validasi_res();
        }

    }

    function validasi_res()
    {
        $id = $this->input->post('id');
        $username = $this->input->post('username');
        $new_password = $this->input->post('new_password');
        $password = md5($new_password);

        $cek_arr = array(
            'user_id' => $id,
            'username' => $username
        );
        $cek_us = $this->Auth_model->cek_psw('user',$cek_arr)->num_rows();
        if($cek_us > 0)
        {
            $cek_upd = $this->Auth_model->cek_upd($id,$password);
            if ($cek_upd) {
                $sess_data = array(
                    'username' => $nama,
                    'status' => 'login'
                );

                $this->session->set_userdata($sess_data);
                echo "<script>alert('Berhasil Login');document.location='" . base_url('dashboard/oke') . "'</script>";
            }

        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

 
}