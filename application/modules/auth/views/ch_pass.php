

<!-- content wrapper -->

<div class="login">
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                CHANGE PASSWORD
            </div>
            <div class="card-body">
                <form action="" method="post" id="form-test">
                <?php form_open('change');?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <?php 
                                    foreach($lumia as $row) { ?>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="hidden" name='id' value="<?php echo $row['user_id']; ?>">
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Username" value="<?php echo $row['username']; ?>">
                                </div>
                                <?= form_error('username','<small class="text-danger pl-3">','</small>'); ?>
                            <?php } ?>
                            </div>
                            <!-- <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" name="password" class="form-control" id="password" placeholder="current password">
                                </div>
                                <?= form_error('password','<small class="text-danger pl-3">','</small>'); ?>
                            </div> -->
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="password" name="new_password" class="form-control" id="password" placeholder="New password">
                                </div>
                                <?= form_error('new_password','<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                            <input type="submit" value="submit" class="btn btn-success" id="nojob">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end -->