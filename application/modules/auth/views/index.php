

<!-- content wrapper -->

<div class="login">
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                LOGIN
            </div>
            <div class="card-body">
                <form action="" method="post">
                <?php form_open('no'); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Username" autocomplete="off">
                                </div>
                                <?php echo form_error('username','<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" autocomplete="off">
                                </div>
                                <?php echo form_error('password','<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                            <input type="submit" value="login" class="btn btn-success" id="nojob">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end -->