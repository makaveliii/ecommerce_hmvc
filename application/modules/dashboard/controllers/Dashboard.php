<?php

class Dashboard extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('status') != 'login') {
            redirect('auth');
        }
    }

    function index()
    {
        $this->load->view('template/header');
        $this->load->view('index');
        $this->load->view('template/footer');
    }

}